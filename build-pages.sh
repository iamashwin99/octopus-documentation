#!/usr/bin/env bash

PREFIX="/new-site"
DOC_TOP=$PWD
OCTOPUS_TOP=$PWD/octopus
OCTOPUS_DOC=$OCTOPUS_TOP/doc/html/
#OCTOPUS_GIT=https://gitlab.com/octopus-code/octopus.git
OCTOPUS_GIT=git@gitlab.com:octopus-code/octopus.git

#DEBUG=' --debug '

if [ -d $OCTOPUS_TOP ];
then
    pushd $OCTOPUS_TOP
        git fetch --all --tags
    popd
else
    git clone $OCTOPUS_GIT
fi


rm *.log
rm -f config_toml.end

grep -v '^#' branches | grep '\S' > branches_stripped

weight_counter=0

while IFS=' ' read -r branch name; do

    let weight_counter=$weight_counter+1
    echo '[[menu.main]]' >> config_toml.end
    echo '  name = "'$name'"' >> config_toml.end
    echo '  branch = "'$branch'"' >> config_toml.end
    echo '  weight = '$weight_counter >> config_toml.end
    echo '' >> config_toml.end

done < branches_stripped

scripts/create_release_pages.py

while IFS=' ' read -r branch name; do

    echo "Building branch: " $branch $name
    cd octopus
    git checkout $branch
    git pull
    cd ..

    rm -rf content/*
    rm -rf static/images

    objects=(
        Manual
        Tutorial
        Developers
        _index.md
        Citing_Octopus.md
        Articles.md
        Links.md
        Books.md
        Octopus_Developers.md
        Octopus_Features.md
        Download.md
        Changelog.md
        FAQ.md
        Octopus_10.md
    ) 


#    for obj in ${objects[@]}; do
#
#        cp -r $PWD/default/content/$obj $PWD/content/
#        if [ -d $PWD/$OCTOPUS_DOC/content/$obj ];
#        then
#            cp -r $PWD/$OCTOPUS_DOC/content/$obj $PWD/content/
#        fi
#
#    done


    cp -r $PWD/default/content/* $PWD/content/
    if [ -d $OCTOPUS_DOC/content ];
    then
        cp -r $OCTOPUS_DOC/content/* $PWD/content/
    fi


    cp -r $PWD/default/images $PWD/static/

    if [ -d $OCTOPUS_DOC/images ];
    then
        cp -r $OCTOPUS_DOC/images/* $PWD/static/images
    fi

    if [ -d $OCTOPUS_DOC/graph_data ];
    then
        cp -r $OCTOPUS_DOC/graph_data $PWD/static
    else
        mkdir $PWD/static/graph_data
    fi

    cp -r $PWD/default/includes $PWD/

    if [ -d $OCTOPUS_DOC/includes ];
    then
        cp -r $OCTOPUS_DOC/includes/* $PWD/includes/
    fi

    rm -rf content/Variables/*

    scripts/parse_variables.py --disable-headers --disable-varinfo --disable-variables -d data/varinfo_$branch.json --docdir=./content/ -s $OCTOPUS_TOP
    scripts/var2html.py --definitions=data/varinfo_$branch.json --variables='content/Variables' 
    scripts/classes.py
    
    sed $DEBUG "{s|OCTOPUSVERSION|$name|g;s|OCTOPUSBRANCH|$branch|g;s|SITE_PREFIX|$PREFIX|g}" config.toml_TEMPLATE > config.toml

    cat config_toml.end >> config.toml

    # Tags are broken in the docdock theme, therefore we need to comment out tags from all pages for now:
    
    for f in content/*.md; do
        sed $DEBUG -i '{s|tags:|#tags:|g}' $f
    done
    for f in content/*/*.md; do
        sed $DEBUG -i '{s|tags:|#tags:|g}' $f
    done
    for f in content/*/*/*.md; do
        sed $DEBUG -i '{s|tags:|#tags:|g}' $f
    done
        for f in content/*/*/*/*.md; do
        sed $DEBUG -i '{s|tags:|#tags|g}' $f
    done


#####

# Include input files:
echo "Processing testfile includes"

grep -r "^#include_input " content/ | sed $DEBUG '{s|:| |g}' > input_lines
grep -r "^#include_input " content/ | sed $DEBUG '{s|{{% ||g}' | cut -d ' ' -f 2 | sed $DEBUG '{s|\"||g}' > include_files
rm -rf static/testsuite

while IFS=' ' read -r testfile; do
    dir=`dirname $testfile`
    outfile=`basename $testfile`
    mkdir -p $PWD/static/$dir
    #echo   "process testfiles "$testfile, $dir, $outfile
    $PWD/scripts/annotate_input.py -d $PWD/data/varinfo_$branch.json -i $OCTOPUS_TOP/$testfile -o $PWD/static/$dir/$outfile
done < include_files

while IFS=' ' read -r mdfile marker testfile; do
    dir=`dirname $testfile`
    outfile=$dir/`basename $testfile`
    #echo   "process testfiles "$mdfile": "$marker" "$outfile
    sed $DEBUG -i -e "\|^$marker $testfile *$|r $PWD/static/$outfile" $mdfile
    sed $DEBUG -i -e "\|^$marker $testfile *$|d" $mdfile
done < input_lines

rm include_files input_lines 

echo "Processing input snippet includes"

grep -r "^#include_input_snippet " content/ | sed $DEBUG '{s|:| |g}' > input_snippet_lines

while IFS=' ' read -r mdfile marker input_file snippet_name; do
    #echo   "process input snippet "$mdfile": "$marker" "$input_file" "$snippet_name

    tmp_file=$PWD/static/`basename $input_file`_$snippet_name.txt
    #echo "tmp_file: "$tmp_file" input_file: "$input_file" snippet_name: "$snippet_name
    sed $DEBUG -n "/^ *\# *snippet_start *$snippet_name *$/,/^ *\# *snippet_end *$/p" $OCTOPUS_TOP/$input_file | grep -v "snippet_start" | grep -v "snippet_end" | $PWD/scripts/annotate_input.py -d $PWD/data/varinfo_$branch.json > $tmp_file

    sed $DEBUG -i -e "\|^$marker $input_file $snippet_name|r $tmp_file" $mdfile
    sed $DEBUG -i -e "\|^$marker $input_file $snippet_name|d" $mdfile

    rm $tmp_file
done < input_snippet_lines

rm input_snippet_lines 



# Include files:
echo "Processing general file includes"

grep -r "^#include_file " content/ | sed $DEBUG '{s|:| |g}' > general_input_lines
grep -r "^#include_file " content/ | sed $DEBUG '{s|{{% ||g}' | cut -d ' ' -f 2 | sed $DEBUG '{s|\"||g}' > include_general_files
rm -rf static/testsuite

while IFS=' ' read -r testfile; do
    dir=`dirname $testfile`
    outfile=`basename $testfile`
    mkdir -p $PWD/static/$dir
    #echo   "process include files "$testfile, $dir, $outfile
    cp $OCTOPUS_TOP/$testfile $PWD/static/$dir/$outfile
done < include_general_files

while IFS=' ' read -r mdfile marker testfile; do
    dir=`dirname $testfile`
    outfile=$dir/`basename $testfile`
    #echo   "process include files "$mdfile": "$marker" "$outfile
    sed $DEBUG -i -e "\|^$marker $testfile *$|r $PWD/$testfile" $mdfile
    sed $DEBUG -i -e "{s|^$marker $testfile *$||g}" $mdfile
done < general_input_lines

#rm include_files input_lines 


# Include tutorial images
echo "Processing eps includes"

grep -r "^#include_eps " content/ | sed $DEBUG '{s|md:|md |g}' > input_eps_lines
grep -r "^#include_eps " content/ | sed $DEBUG '{s|{{% ||g}' | cut -d ' ' -f 2 | sed $DEBUG '{s|\"||g}' > include_eps_files

while IFS=' ' read -r epsfile; do
    dir=`dirname $epsfile`
    outfile=`basename $epsfile eps`png
    mkdir -p $PWD/static/$dir
    echo   "process eps files "$epsfile, $dir, $outfile
    convert -rotate 90 -flatten -density 300 -size 3300x2550 $OCTOPUS_TOP/$epsfile $PWD/static/$dir/$outfile
done < include_eps_files

while IFS=' ' read -r mdfile marker epsfile caption; do
    dir=`dirname $epsfile`
    outfile=/$dir/`basename $epsfile eps`png
    if [[ ! $caption == *"width"* ]]; then
        new_caption=$caption" width=\"500px\""
    else
        new_caption=$caption
    fi
    echo   "process eps files "$mdfile": "$marker" "$outfile" |"$new_caption"|"
    sed $DEBUG -i -e "\|^$marker $epsfile $caption *$|c {{<figure src=\"$outfile\" $new_caption >}}" $mdfile
done < input_eps_lines

# Include type definitions:
echo "Processing type_def includes"

grep -r "^#include_type_def" content/ | sed $DEBUG '{s|:| |g}' > def_input_lines
grep -r "^#include_type_def" content/ | cut -d ' ' -f 2 | sed $DEBUG '{s|\"||g}' > type_names

while IFS=' ' read -r typename; do
    sed $DEBUG -n "/^ *type.* $typename *$/,/end type/p" $OCTOPUS_TOP/src/*/*.F90 > $PWD/static/$typename.txt
done < type_names

while IFS=' ' read -r mdfile marker typename; do
    #echo   "process def "$mdfile": "$marker" "$typename
    sed $DEBUG -i -e "\|^$marker $typename *$|r $PWD/static/$typename.txt" $mdfile
    sed $DEBUG -i -e "{/^$marker $typename *$/d}" $mdfile
done < def_input_lines

rm type_names def_input_lines 

# Include functions:
echo "Processing function includes"

grep -r "^#include_function" content/ | sed $DEBUG '{s|:| |g}' > function_input_lines
grep -r "^#include_function" content/ | cut -d ' ' -f 2 | sed $DEBUG '{s|\"||g}' > function_names

while IFS=' ' read -r functionname; do
    sed $DEBUG -n "/function *$functionname(/,/end function/p" $OCTOPUS_TOP/src/*/*.F90 > $PWD/static/$functionname.txt
done < function_names

while IFS=' ' read -r mdfile marker functionname; do
    #echo   "process function "$mdfile": "$marker" "$functionname
    sed $DEBUG -i -e "\|^$marker $functionname *$|r $PWD/static/$functionname.txt" $mdfile
    sed $DEBUG -i -e "{/^$marker $functionname *$/d}" $mdfile
done < function_input_lines

rm function_names function_input_lines 

# Include subroutines:
echo "Processing subroutine includes"

grep -r "^#include_subroutine" content/ | sed $DEBUG '{s|:| |g}' > subroutine_input_lines
grep -r "^#include_subroutine" content/ | cut -d ' ' -f 2 | sed $DEBUG '{s|\"||g}' > subroutine_names

while IFS=' ' read -r subroutinename; do
    sed $DEBUG -n "/subroutine *$subroutinename(/,/end subroutine/p" $OCTOPUS_TOP/src/*/*.F90 > $PWD/static/$subroutinename.txt
done < subroutine_names

while IFS=' ' read -r mdfile marker subroutinename; do
    #echo   "process sub "$mdfile": "$marker" "$subroutinename
    sed $DEBUG -i -e "\|^$marker $subroutinename *$|r $PWD/static/$subroutinename.txt" $mdfile
    sed $DEBUG -i -e "{/^$marker $subroutinename *$/d}" $mdfile
done < subroutine_input_lines

rm subroutine_names subroutine_input_lines 

# Include code marked snippets
echo "Processing code snippet includes"

grep -r "^#include_code_doc" content/ | sed $DEBUG '{s|:| |g}' > doc_input_lines
grep -r "^#include_code_doc" content/ | cut -d ' ' -f 2 | sed $DEBUG '{s|\"||g}' > doc_names

while IFS=' ' read -r docname; do
    sed $DEBUG -n "/^ *\!\# *doc_start *$docname *$/,/^ *\!\# *doc_end *$/p" $OCTOPUS_TOP/src/*/*.F90 | grep -v "doc_start" | grep -v "doc_end" > $PWD/static/doc_$docname.txt
done < doc_names

while IFS=' ' read -r mdfile marker docname; do
    #echo   "process code snippet "$mdfile": "$marker" "$docname
    sed $DEBUG -i -e "\|^$marker $docname|r $PWD/static/doc_$docname.txt" $mdfile
    sed $DEBUG -i -e "{s|^$marker $docname||g}" $mdfile
done < doc_input_lines

rm doc_names doc_input_lines



#####


    rm -rf public/$name
    rm -rf public/$PREFIX/$name

    echo "running hugo"
    hugo --destination "public/$PREFIX/$name"  > $name.log 2>&1
    echo "Hugo exited with " $?

    rm -rf static/images static/testsuite static/doc static/octopus static/includes
    rm -rf static/*.txt
    rm -rf static/graph_data


    rm config.toml

done < branches_stripped

# Clean up:

rm config_toml.end
rm -rf content/*

