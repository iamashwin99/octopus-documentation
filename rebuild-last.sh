#!/usr/bin/env bash

if [[ -z "${OCTOPUS_TOP}" ]]; then
    OCTOPUS_TOP=$PWD/octopus
fi

OCTOPUS_DOC=$OCTOPUS_DOCs/doc/html/
PREFIX="/new-site"


rm -f *.log
rm -f config_toml.end

grep -v '^#' branches | grep '\S' > branches_stripped

weight_counter=0

while IFS=' ' read -r loc_branch loc_name; do

    name=$loc_name
    branch=$loc_branch

    let weight_counter=$weight_counter+1
    echo '[[menu.main]]' >> config_toml.end
    echo '  name = "'$name'"' >> config_toml.end
    echo '  branch = "'$branch'"' >> config_toml.end
    echo '  weight = '$weight_counter >> config_toml.end
    echo '' >> config_toml.end

done < branches_stripped


echo "Building branch: " $branch $name

if [ -d $PWD/content/ ]; then
    rm -rf $PWD/content/*
else
    mkdir $PWD/content
fi

rm -rf static/images

objects=(
    Manual
    Tutorial
    Developers
    _index.md
    Octopus_Features.md
    Citing_Octopus.md
    Articles.md
    Links.md
    Books.md
    Octopus_Developers.md
    FAQ.md
    Download.md
    Changelog.md
    Octopus_10.md
) 


cp -r $PWD/default/content/* $PWD/content/
echo "copying: "$PWD/default/content/

if [ -d $PWD/$OCTOPUS_DOC/content ];
then
    cp -r $PWD/$OCTOPUS_DOC/content/* $PWD/content/
fi


cp -r $PWD/default/images $PWD/static/

if [ -d $PWD/$OCTOPUS_DOC/images ];
then
    cp -r $PWD/$OCTOPUS_DOC/images/* $PWD/static/images
fi

if [ -d $PWD/$OCTOPUS_DOC/graph_data ];
then
    cp -r $PWD/$OCTOPUS_DOC/graph_data $PWD/static/
else
    mkdir $PWD/static/graph_data
fi

cp -r $PWD/default/includes $PWD/

if [ -d $PWD/$OCTOPUS_DOC/includes ];
then
    cp -r $PWD/$OCTOPUS_DOC/includes/* $PWD/includes/
fi



rm -rf content/Variables/*

./scripts/parse_variables.py --disable-headers --disable-varinfo --disable-variables -d data/varinfo_$branch.json --docdir=./content/ -s $OCTOPUS_TOP
./scripts/var2html.py --definitions=data/varinfo_$branch.json --variables='content/Variables' 
./scripts/classes.py

sed "{s|OCTOPUSVERSION|$name|g;s|OCTOPUSBRANCH|$branch|g;s|SITE_PREFIX|$PREFIX|g}" config.toml_TEMPLATE > config.toml

cat config_toml.end >> config.toml


grep -r "^#include_input " content/ | sed '{s|:| |g}' > input_lines
grep -r "^#include_input " content/ | sed '{s|{{% ||g}' | cut -d ' ' -f 2 | sed '{s|\"||g}' > include_files
rm -rf static/testsuite

while IFS=' ' read -r testfile; do
    dir=`dirname $testfile`
    outfile=`basename $testfile`
    mkdir -p $PWD/static/$dir
    #echo   "process testfiles "$testfile, $dir, $outfile
    $PWD/scripts/annotate_input.py -d $PWD/data/varinfo_$branch.json -i $OCTOPUS_TOP/$testfile -o $PWD/static/$dir/$outfile
done < include_files

while IFS=' ' read -r mdfile marker testfile; do
    dir=`dirname $testfile`
    outfile=$dir/`basename $testfile`
    #echo   "process testfiles "$mdfile": "$marker" "$outfile
    sed -i -e "\|^$marker $testfile *$|r $PWD/static/$outfile" $mdfile
    sed -i -e "\|^$marker $testfile *$|d" $mdfile
done < input_lines

rm include_files input_lines 


# Include files:

grep -r "^#include_file " content/ | sed '{s|:| |g}' > general_input_lines
grep -r "^#include_file " content/ | sed '{s|{{% ||g}' | cut -d ' ' -f 2 | sed '{s|\"||g}' > include_general_files
rm -rf static/testsuite

while IFS=' ' read -r testfile; do
    dir=`dirname $testfile`
    outfile=`basename $testfile`
    mkdir -p $PWD/static/$dir
    #echo   "process include files "$testfile, $dir, $outfile
    cp $PWD/$testfile $PWD/static/$dir/$outfile
done < include_general_files

while IFS=' ' read -r mdfile marker testfile; do
    dir=`dirname $testfile`
    outfile=$dir/`basename $testfile`
    #echo   "process include files "$mdfile": "$marker" "$outfile
    sed -i -e "\|^$marker $testfile *$|r $OCTOPUS_TOP/$testfile" $mdfile
    sed -i -e "{s|^$marker $testfile *$||g}" $mdfile
done < general_input_lines

rm include_files input_lines 


# Include tutorial images

grep -r "^#include_eps " content/ | sed '{s|:| |g}' > input_eps_lines
grep -r "^#include_eps " content/ | sed '{s|{{% ||g}' | cut -d ' ' -f 2 | sed '{s|\"||g}' > include_eps_files

while IFS=' ' read -r epsfile; do
    dir=`dirname $epsfile`
    outfile=`basename $epsfile eps`png
    mkdir -p $PWD/static/$dir
    #echo   "process eps files "$epsfile, $dir, $outfile
    convert -rotate 90 -flatten -density 300 -size 3300x2550 $OCTOPUS_TOP/$epsfile $PWD/static/$dir/$outfile
done < include_eps_files

while IFS=' ' read -r mdfile marker epsfile caption; do
    dir=`dirname $epsfile`
    outfile=/$dir/`basename $epsfile eps`png
    if [[ ! $caption == *"width"* ]]; then
        new_caption=$caption" width=\"500px\""
    else
        new_caption=$caption
    fi
    #echo   "process eps files "$mdfile": "$marker" "$outfile" |"$new_caption"|"
    sed -i -e "\|^$marker $epsfile $caption *$|c {{<figure src=\"$outfile\" $new_caption >}}" $mdfile
done < input_eps_lines

# Include type definitions:

grep -r "^#include_type_def" content/ | sed '{s|:| |g}' > def_input_lines
grep -r "^#include_type_def" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > type_names

while IFS=' ' read -r typename; do
    sed -n "/^ *type.* $typename *$/,/end type/p" octopus/src/*/*.F90 > $PWD/static/$typename.txt
done < type_names

while IFS=' ' read -r mdfile marker typename; do
    #echo   "process def "$mdfile": "$marker" "$typename
    sed -i -e "\|^$marker $typename *$|r $PWD/static/$typename.txt" $mdfile
    sed -i -e "{/^$marker $typename *$/d}" $mdfile
done < def_input_lines

rm type_names def_input_lines 

# Include functions:

grep -r "^#include_function" content/ | sed '{s|:| |g}' > function_input_lines
grep -r "^#include_function" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > function_names

while IFS=' ' read -r functionname; do
    sed -n "/function *$functionname *(/,/end function/p" octopus/src/*/*.F90 > $PWD/static/$functionname.txt
done < function_names

while IFS=' ' read -r mdfile marker functionname; do
    #echo   "process function "$mdfile": "$marker" "$functionname
    sed -i -e "\|^$marker $functionname *$|r $PWD/static/$functionname.txt" $mdfile
    sed -i -e "{/^$marker $functionname *$/d}" $mdfile
done < function_input_lines

#rm function_names function_input_lines 

# Include subroutines:

grep -r "^#include_subroutine" content/ | sed '{s|:| |g}' > subroutine_input_lines
grep -r "^#include_subroutine" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > subroutine_names

while IFS=' ' read -r subroutinename; do
    sed -n "/subroutine *$subroutinename *(/,/end subroutine/p" $OCTOPUS_TOP/src/*/*.F90 > $PWD/static/$subroutinename.txt
done < subroutine_names

while IFS=' ' read -r mdfile marker subroutinename; do
    #echo   "process sub "$mdfile": "$marker" "$subroutinename
    sed -i -e "\|^$marker $subroutinename *$|r $PWD/static/$subroutinename.txt" $mdfile
    sed -i -e "{/^$marker $subroutinename *$/d}" $mdfile
done < subroutine_input_lines

rm subroutine_names subroutine_input_lines 

# Include code marked snippets

grep -r "^#include_code_doc" content/ | sed '{s|:| |g}' > doc_input_lines
grep -r "^#include_code_doc" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > doc_names

while IFS=' ' read -r docname; do
    sed -n "/^ *\!\# *doc_start *$docname *$/,/^ *\!\# *doc_end *$/p" $OCTOPUS_TOP/src/*/*.F90 | grep -v "doc_start" | grep -v "doc_end" > $PWD/static/doc_$docname.txt
done < doc_names

while IFS=' ' read -r mdfile marker docname; do
    #echo   "process code snippet "$mdfile": "$marker" "$docname
    sed -i -e "\|^$marker $docname|r $PWD/static/doc_$docname.txt" $mdfile
    sed -i -e "{s|^$marker $docname||g}" $mdfile
done < doc_input_lines

rm doc_names doc_input_lines

grep -r "^#include_input_snippet " content/ | sed '{s|:| |g}' > input_snippet_lines

while IFS=' ' read -r mdfile marker input_file snippet_name; do
    #echo   "process input snippet "$mdfile": "$marker" "$input_file" "$snippet_name

    tmp_file=$PWD/static/`basename $input_file`_$snippet_name.txt
    #echo $tmp_name" "$input_file" "$snippet_name
    sed -n "/^ *\# *snippet_start *$snippet_name *$/,/^ *\# *snippet_end *$/p" $OCTOPUS_TOP/$input_file | grep -v "snippet_start" | grep -v "snippet_end" | $PWD/scripts/annotate_input.py -d $PWD/data/varinfo.json > $tmp_file

    sed -i -e "\|^$marker $input_file $snippet_name|r $tmp_file" $mdfile
    sed -i -e "\|^$marker $input_file $snippet_name|d" $mdfile

    rm $tmp_file
done < input_snippet_lines

rm input_snippet_lines 

rm -rf public/$PREFIX/$name

hugo --destination "public/$PREFIX/$name"  > $name.log 2>&1
echo "Hugo exited with " $?


rm config.toml

rm config_toml.end
rm -rf content/*
rm -rf static/testsuite static/doc static/octopus static/includes
rm -rf static/*.txt
rm -rf static/images
rm -rf static/graph_data

