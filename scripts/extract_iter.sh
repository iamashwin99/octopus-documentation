#!/usr/bin/env bash

num_iter=`awk '/SCF converged/{printf "%5d", $5}' $1`


sed -n "/<><><>/,/Calculation started/p" $1 > header.txt
sed -n "/SCF CYCLE ITER #    1/,/\*\*\*\*/p" $1 > first_iter.txt
sed -n "/SCF CYCLE ITER #$num_iter/,/\*\*\*\*/p" $1 > last_iter.txt
sed -n "/Info: Writing states/,//p" $1 > footer.txt
