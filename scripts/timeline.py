#!/usr/bin/env python3

import re
import sys
import os
import json
import getopt


def usage():
    """print usage information."""

    print('Usage: timeline [options]')
    print('')
    print('Opdions:')
    print('  -h, --help              Prints this help and exits.')
    print('  -i, --input=NAME   Name of the input file (defaults to stdin)')
    print('  -o, --output=NAME  Name of the JSON output file')

def make_ID(i):
    return 'step_'+format( i, '08d' )

try:
    options, args = getopt.getopt(sys.argv[1:], "i:o:h", ['input=',  'output=', 'help'] )
except  getopt.GetoptError:
    print('Error !')
    usage()
    sys.exit(-1)

sourcefile = "-"
outputfile = "-"

for (opt, arg) in options:
    if opt in ['-h', '--help']:
        usage()
        sys.exit(0)
    if opt in ['-i', '--input']:
        sourcefile = arg
    if opt in ['-o', '--output']:
        outputfile = arg


if sourcefile is '-':
    source = sys.stdin
else:
    source = open(sourcefile,'r')

if outputfile is '-':
    propagation_json = sys.stdout
else:
    propagation_json = open(outputfile,'w')


propagation  = dict()
systems      = dict()
ID_stack     = list()
system_stack = list()


for line in source:

    words = line.split('|')
    (mode, id) = words[0].split(':')


    if 'IN' in mode:
        key_ID = make_ID( int(id) )

        new_entry = dict()
        new_entry['in_ID']=int(id)
        new_entry['children']=list()
        new_entry['parent']=list()
        new_entry['system_clock']=list()
        new_entry['prop_clock']=list()
        new_entry['interaction_clock']=list()
        new_entry['partner_clock']=list()
        new_entry['requested_clock']=list()

        for pair in words[1:]:
            (key, val) = pair.split(':')
            if key.strip() == 'system_clock':
                new_entry['system_clock'].append(val.strip())
            elif key.strip() == 'prop_clock':
                new_entry['prop_clock'].append(val.strip())
            elif key.strip() == 'interaction_clock':
                new_entry['interaction_clock'].append(val.strip())
            elif key.strip() == 'partner_clock':
                new_entry['partner_clock'].append(val.strip())
            elif key.strip() == 'requested_clock':
                new_entry['requested_clock'].append(val.strip())
            else:
                new_entry[key.strip()] = val.strip()

        if len(ID_stack)>0:
            new_entry['parent'].append(make_ID(ID_stack[-1]))
            propagation[make_ID(ID_stack[-1])]['children'].append(make_ID(int(id)))

        propagation[key_ID] = new_entry

        if new_entry['system'] == 'KEEP':
            new_entry['system'] = system_stack[-1]

        system_stack.append(new_entry['system'])

        new_entry['system'] = 'root'+new_entry['system']

        system_name = new_entry['system']
            
        if system_name not in systems:
            systems[system_name] = dict()
            systems[system_name]['interactions'] = list()
            systems[system_name]['children'] = list()
            systems[system_name]['parent'] = list()

        if 'target' in new_entry.keys():
            tmp = new_entry['target'].split('-')
            interaction = {'type': tmp[0], 'partner': tmp[1]}
            if interaction not in systems[new_entry['system']]['interactions']:
                systems[new_entry['system']]['interactions'].append(interaction)

        ID_stack.append(int(id))


    if 'OUT' in mode:
        ID_stack.pop()
        system_stack.pop()

        for pair in words[1:]:
            (key, val) = pair.split(':')
            if 'closes' in key:
                enter_ID = make_ID( int(val) )
                propagation[enter_ID]['out_ID'] = int(id)
            if 'update' in key:
                propagation[enter_ID]['updated'] = val.strip()
            if key.strip() == 'system_clock':
                propagation[enter_ID]['system_clock'].append(val.strip())
            if key.strip() == 'prop_clock':
                propagation[enter_ID]['prop_clock'].append(val.strip())
            if key.strip() == 'interaction_clock':
                propagation[enter_ID]['interaction_clock'].append(val.strip())
            if key.strip() == 'partner_clock':
                propagation[enter_ID]['partner_clock'].append(val.strip())
            if key.strip() == 'requested_clock':
                propagation[enter_ID]['requested_clock'].append(val.strip())

    if 'MARKER' in mode:

        key_ID = make_ID( int(id) )

        new_entry = dict()
        new_entry['in_ID']=int(id)
        new_entry['children']=list()
        new_entry['parent']=list()
        new_entry['system_clock']=list()
        new_entry['prop_clock']=list()
        new_entry['interaction_clock']=list()
        new_entry['partner_clock']=list()
        new_entry['requested_clock']=list()


        for pair in words[1:]:
            (key, val) = pair.split(':')
            if key.strip() == 'type':
                new_entry['function'] = val.strip()
            if key.strip() == 'system_clock':
                new_entry['system_clock'].append(val.strip())
            elif key.strip() == 'prop_clock':
                new_entry['prop_clock'].append(val.strip())
            elif key.strip() == 'interaction_clock':
                new_entry['interaction_clock'].append(val.strip())
            elif key.strip() == 'partner_clock':
                new_entry['partner_clock'].append(val.strip())
            elif key.strip() == 'requested_clock':
                new_entry['requested_clock'].append(val.strip())
            else:
                new_entry[key.strip()] = val.strip()

        if len(ID_stack)>0:
            new_entry['parent'].append(make_ID(ID_stack[-1]))
            propagation[make_ID(ID_stack[-1])]['children'].append(make_ID(int(id)))

        propagation[key_ID] = new_entry

        if new_entry['system'] == 'KEEP':
            new_entry['system'] = system_stack[-1]


        new_entry['system'] = 'root'+new_entry['system']

        system_name = new_entry['system']
            
        if system_name not in systems:
            systems[system_name] = dict()
            systems[system_name]['interactions'] = list()
            systems[system_name]['children'] = list()
            systems[system_name]['parent'] = list()

        if 'target' in new_entry.keys():
            tmp = new_entry['target'].split('-')
            interaction = {'type': tmp[0], 'partner': tmp[1]}
            if interaction not in systems[new_entry['system']]['interactions']:
                systems[new_entry['system']]['interactions'].append(interaction)





for system in systems.keys():
    names = system.split('.')
    if len(names) == 2:
        systems['root']['children'].append(system)
    if len(names) > 2:
        systems['.'.join(names[0:-1])]['children'].append(system)
    if len(names)>1:
        systems[system]['parent'].append('.'.join(names[0:-1]))



json_data = dict()

json_data['events']  = propagation
json_data['systems'] = systems

print(json.dumps(json_data, indent=2), file=propagation_json)
propagation_json.close()
