#!/usr/bin/env bash

rm -rf content/Variables/*

./scripts/parse_variables.py --disable-headers --disable-varinfo --disable-variables -d data/varinfo.json --docdir=./content/ -s octopus
./scripts/var2html.py --definitions=data/varinfo.json --variables='content/Variables' 
./scripts/classes.py -s octopus/src/
