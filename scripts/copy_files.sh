#!/usr/bin/env bash


OCTOPUS_DOC=octopus/doc/html/

if [ ! -d $PWD/content ];
then
    mkdir content
fi

if [ ! -d $PWD/includes ];
then
    mkdir includes
fi

rm -rf content/*
rm -rf static/images


cp -r $PWD/default/content/* $PWD/content/
if [ -d $PWD/$OCTOPUS_DOC/content ];
then
    cp -r $PWD/$OCTOPUS_DOC/content/* $PWD/content/
fi

cp -r $PWD/default/images $PWD/static/
if [ -d $PWD/$OCTOPUS_DOC/images ];
then
    cp -r $PWD/$OCTOPUS_DOC/images/* $PWD/static/images
fi

if [ -d $PWD/$OCTOPUS_DOC/graph_data ];
then
    cp -r $PWD/$OCTOPUS_DOC/graph_data $PWD/static
else
    mkdir -p $PWD/static/graph_data
fi

cp -r $PWD/default/includes $PWD/
if [ -d $PWD/$OCTOPUS_DOC/includes ];
then
    cp -r $PWD/$OCTOPUS_DOC/includes/* $PWD/includes/
fi
