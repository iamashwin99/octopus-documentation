#!/usr/bin/env python3

# Copyright (C) 2020 Martin Lueders 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

import sys
import os.path
import glob
import json
import getopt
from variables import Variables

def usage():
    """print usage information."""

    print('Usage: var2html [options]')
    print('')
    print('Opdions:')
    print('  -h, --help              Prints this help and exits.')
    print('  -v, --versbose          Print names of parsed files.')
    print('  -d, --definitions=NAME  Name of the JSON definitions file')
    print('  --variables-dir=DIR     Name of the top Variables directory')


def highlight_letter(a, b):

    if a == b:
        return "<b>"+a+"</b>"
    else:
        return a



def create_section(prefix, section, weight, alpha):
    """create the header for a (sub)-section."""

    path = prefix+section

    section_dir = variables_top_dir+'/'+path.replace(' ','_')

#    print('creating ', section_dir)

    if not os.path.isdir(section_dir):
        os.mkdir(section_dir)

    section_index = open(section_dir+'/_index.md', 'w')

    print('---', file=section_index)
    print('Title: "'+section.strip(' ')+'"',file=section_index)
    print('section: "Input Variables"', file=section_index)
    print('Weight: ', weight, file=section_index)
    if(alpha):
        print('Hidden: True', file=section_index)
    print('---\n', file=section_index)

    letters = list()
    for letter in alphabetic.keys():
        letters.append('{{< versioned-link-hl "variables/Index/' + letter + '"  "'+letter+'" "'+section+'" >}}')
    print('<div align="center">', file=section_index)
    print('  -  '.join(letters), file=section_index, end='' )
    print('</div>', file=section_index)
    print('<br>\n\n', file=section_index)

    return section_index




def traverse(prefix, current_section_file, mytree, alpha=False):
    """recursive funnction to traverse a tree."""

    weight = 10
    for a in sorted(mytree.keys()):
        if(type(mytree[a])==dict):
            section = a.strip(' ')
            weight += 1
            section_file = create_section(prefix, section.lstrip('/'), weight, alpha)
            if (current_section_file):

                section_string = '* {{< versioned-link "variables/' +(prefix+'/'+section).replace(' ','_')+ '"  "'+section.lstrip('/')+'" >}}'
                print(section_string+'<br>\n\n', file=current_section_file )

            traverse(prefix+section+'/', section_file, mytree[section], alpha)
            section_file.close()
        else:
            var = a.strip(' ')
            if not alpha:
                weight += 1
                variables_file = open(variables_top_dir+'/'+prefix.replace(' ','_')+var+'.md','w')
                write_variable(var, variables_file, weight)
                variables_file.close()

            if (current_section_file):
                print('{{%expand "'+variables[var]['Name']+'"%}}\n',file=current_section_file)
                write_variable(var, current_section_file, weight, head=False)
                print('{{%/expand%}}<br>\n',file=current_section_file)



def write_variable(var, variables_file, weight, head=True):
    """write a single variable entry to a file"""

    if(head):
        print('---', file=variables_file)
        print('Title: '+variables[var]['Name'],file=variables_file)
        print('section: "Input Variables"', file=variables_file)
        print('Weight: ', weight, file=variables_file)
        print('---\n', file=variables_file)


        letters = list()
        for letter in alphabetic.keys():
            letters.append('{{< versioned-link "variables/Index/' + letter + '"  "'+letter+'" >}}')
        print('<div align="center">', file=variables_file)
        print('  -  '.join(letters), file=variables_file, end='' )
        print('</div>', file=variables_file)
        print('</br>\n', file=variables_file)


    variables.write_variable_md(var, file=variables_file, head=head)
    print('<hr>\n', file=variables_file)

    if(head):
        print('{{%expand "Source information"%}}',file=variables_file)
        sourcefile = variables[var]['Sourcefile']
        if 'LineNumber' in variables[var]:
            linenumber = ': '+str(variables[var]['LineNumber'])
        else:
            linenumber = ''
        print('<a href="https://gitlab.com/octopus-code/octopus/-/blob/{{< git-branch >}}/src/'+sourcefile+'">'+sourcefile+'</a> '+linenumber  ,file=variables_file)
        if 'CallLine' in variables[var]:
            print('```Fortran',file=variables_file)
            print(' '+variables[var]['CallLine'],file=variables_file)
            print('```',file=variables_file)
    
        print('{{%/expand%}}\n',file=variables_file)
    
        if 'Tutorials' in variables[var]:
            if len(variables[var]['Tutorials']) > 0:
                print('<br>',file=variables_file)
                print('{{%expand "Featured in tutorials"%}}',file=variables_file)
                print('<ul>',file=variables_file)
                for tutorial in variables[var]['Tutorials']:
                    print('<li> {{< tutorial "'+tutorial+'" "'+tutorial+'" >}} </li>',file=variables_file)
                print('</ul>',file=variables_file)
                print('{{%/expand%}}',file=variables_file)

        if 'Manuals' in variables[var]:
            if len(variables[var]['Manuals']) > 0:
                print('<br>',file=variables_file)
                print('{{%expand "Featured in chapters of the manual:"%}}',file=variables_file)
                print('<ul>',file=variables_file)
                for manual in variables[var]['Manuals']:
                    print('<li> {{< manual "'+manual+'" "'+manual+'" >}} </li>',file=variables_file)
                print('</ul>',file=variables_file)
                print('{{%/expand%}}',file=variables_file)

        if 'Testfiles' in variables[var]:
            if len(variables[var]['Testfiles']) > 0:
                print('<br>',file=variables_file)
                print('{{%expand "Featured in testfiles"%}}',file=variables_file)
                print('<ul>',file=variables_file)
                for testfile in variables[var]['Testfiles']:
                    print('<li><a href="https://gitlab.com/octopus-code/octopus/-/blob/{{< git-branch >}}/testsuite/'+testfile+'">'+testfile+'</a></li>',file=variables_file)
                print('</ul>',file=variables_file)
                print('{{%/expand%}}',file=variables_file)



#================================================================

variables_top_dir = 'Variables'
verbose=False

try:
    options, args = getopt.getopt(sys.argv[1:], "hd:v", ["help", "definitions=", "variables=", "verbose"])
except  getopt.GetoptError:
    usage()
    sys.exit(-1)

for (opt, arg) in options:
    if opt in ['-v', '--verbose']:
        verbose = True
    elif opt in ['-d', '--definitions']:
        variable_defs_name = arg
    elif opt in ['--variables']:
        variables_top_dir = arg

# Read Variable definitions from JSON:

variables = Variables()
variables.import_json(variable_defs_name)

if verbose:
    print("Read definitions from "+variable_defs_name)

if not os.path.isdir(variables_top_dir):
    os.mkdir(variables_top_dir)

if not os.path.isdir(variables_top_dir+'/Index'):
    os.mkdir(variables_top_dir+'/Index')

top_index = open(variables_top_dir+'/_index.md','w')

print("""
---
Title: "Input Variables"
section: "Input Variables"
weight: 30
menu: "top"
description: " "
---

""", file=top_index)


alpha_index = open(variables_top_dir+'/Index/_index.md','w')

print("""
---
Title: "Alphabetic Index"
section: "Input Variables"
weight: 500
description: " "
---


""", file=alpha_index)



file = sys.stdout


sections = dict()
sections_done = set()

# First we build a tree withg respect to sections:


tree = dict()

alphabetic = dict()
index_letters = list()

for var in variables.keys():

    variable = variables[var]

    section_name = variable['Section'].strip(':')
    full_section = section_name.split('::')

    tmp_tree = tree

    length = len(full_section)
    for level in range(0, length):

        section_name = '/'+full_section[level].strip(' ')
        if section_name not in tmp_tree:
            tmp_tree[section_name] = dict()

        tmp_tree = tmp_tree[section_name]

    tmp_tree[var] = var


    first_letter = variables[var]['Name'][0].upper()
    if first_letter not in alphabetic:
        alphabetic[first_letter] = dict()
        index_letters.append('{{< versioned-link "variables/Index/' + first_letter + '"  "'+first_letter+'" >}}')
    
    alphabetic[first_letter][var] = var

print('<div align="center">', file=alpha_index)
print('  -  '.join(index_letters), file=alpha_index, end='' )
print('</div>', file=alpha_index)


traverse('', None, tree)

traverse('Index/', None, alphabetic, alpha=True)

print('<div align="center">', file=top_index)
print('  -  '.join(index_letters), file=top_index, end='' )
print('</div>', file=top_index)
print("""

{{% children depth=5 %}}

""", file=top_index
)

top_index.close()
alpha_index.close()
