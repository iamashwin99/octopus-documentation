#!/usr/bin/env python3

# Copyright (C) 2020 Martin Lueders 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

import sys
import getopt
import json
import re

def usage():
    """
    Print usage information and exit.
    """

    print('annotate_input.py [options]:')
    print('  -i <inputfile>   Input file name. If empty, input is read from stdin.')
    print('  -o <outputfile>  Output file name. If empty, output is written to stdout.')
    print('  -m <mode>        Currently implemented: OctopusWiki.')
    print('  -d <name>        Name of the JSON definitions file')
    print('If no input out output files are specified, stdin/stdout are used.')
    quit()


def annotate(str, mode='OctopusWiki'):
    """
    Return annotated string for variable name.

    Arguments:
    str:  string containing a word of the input file
    mode: annotation mode. Currently implemented:
    - 'OctopusWiki': '{{variable|key|section}}'
    - 'Hugo': '{{< variable "key" "section" >}}'
    - 'Test': 'Function(key, section)'
    """

    key = str.strip().split('.')[-1]
    prefix = str.strip().rstrip(key)

    if mode == 'OctopusWiki':
        section = variables[key.lower()]['Section'].split('::')[0]
        return prefix+'{{variable|'+key+'|'+section+'}}'

    if mode == 'Hugo':

        return prefix+'{{< variable "'+key+'" >}}'

    # Here we can add other formats, e.g. for MkDocs

    if mode == 'Test':
        section = variables[key.lower()]['Section']
        return 'Function('+key+','+section+')'



implemented_modes = ['OctopusWiki', 'Hugo', 'Test']

# Some default names:

prefix = ''
sharedir = prefix+'/share/octopus/'

input_file_name = '-'
output_file_name = '-'
variable_defs_name = sharedir + 'varinfo.json'
mode = 'Hugo'


# Parse command line options:

try:
    options, args = getopt.getopt(sys.argv[1:], "i:o:d:m:")
except  getopt.GetoptError:
    usage()
    sys.exit(-1)

for (opt, arg) in options:
    if opt == '-i':
        input_file_name = arg
    elif opt == '-o':
        output_file_name = arg
    elif opt in ['-d', 'definitions']:
        variable_defs_name = arg
    elif opt == '-m':
        if arg in implemented_modes:
            mode = arg
        else:
            print('Unsopported mode: \''+arg+'\'. Try one of: '+', '.join(implemented_modes))
            sys.exit(-1)
        
if input_file_name == '-':
    if len(args)>0:
        input_file_name = args[0]

if mode == 'Hugo':
    prefix = ''
else:
    prefix = ' '

# Read Variable definitions from JSON:

varinfo_json = open(variable_defs_name,'r')
variables = json.load(varinfo_json)
varinfo_json.close()

# Open input and output files:

if input_file_name == '-':
    input = sys.stdin
else:
    try:
        input = open(input_file_name,'r')
    except FileNotFoundError:
        print(input_file_name+' not found.')
        sys.exit(-1)

if output_file_name == '-':
    output = sys.stdout
else:
    output = open(output_file_name, 'w')

# Process input file:

for line in input:
    if not "snippet_" in line:
        new_line = line
        words = line.split()
        for i in range(0,len(words)):
            # Remove prefix '%':
            if words[i].strip()[0] =='%':
                words[i] = words[i].replace('%','')
            # Annotate, if word is defined variable:
            if words[i].split('.')[-1].lower() in variables.keys():
                new_line = new_line.replace(words[i],annotate(words[i], mode=mode) )
        print(new_line, file=output, end='')


input.close()

output.close()
