---
title: "Workflow"
section: "Developers"
weight: 3
---


In {{< octopus >}} the development and releases are done using Oneflow. This workflow is described [here](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)

In Oneflow there is one eternal branches, {{< file "main" >}}. There are also three types of short-lived branches called supporting branches. These are the feature, release and hotfix branches. For a detailed explanation of the purpose of each type of branch and of the workflow used to create and merge the short-lived branches, please read the Gitflow description from the above link carefully. Nevertheless, there are some points that need to be stressed, while some special cases need to be explained more in detail. 

###  What main branch should be used to create the supporting branches  

One common mistake when using Gitflow is to create a support branch from the wrong main branch, so here is a quick recap: 
* Feature: The feature branches are used to add new features to {{< file "main" >}}, so they must be created from {{< file "main" >}}.
* Release: The release branches need to include all the new features since the previous release, so they must be created from the {{< file "main" >}} branch.
* Hotfix: The purpose of a hotfix branch is to create a new release including fixes for one or more bugs found in the previous release, but without including any new features. The {{< file "main" >}} branch includes new features, so it cannot be used for this. Therefore these branches must be created from the {{< file "release" >}} branch (or tag, if the branch is already deleted after the release).

###  What to do when finding a bug  

If you are working on the {{< file "main" >}} branch or a feature branch, the first thing to do is check if the bug is also present in last release. This will be the most likely situation unless you are working on some feature that was only recently added. 

* The bug is '''not''' present in the release. In this case there is no need for a hotfix branch. Just fix the bug using a feature branch. This can be either a new branch just for the bugfix, or an existing feature branch that is somehow related to the bug. Please do not use a completely unrelated branch to include the bug fix. It is also better if you apply the fix in a commit (or a series of commits if the fix is particularly complex) without including other changes not related to the fix. Make sure that in this case the branch does not contain the word {{< file "hotfix" >}}, otherwise the {{< buildbot >}} will treat the branch as if it was going to be a release branch, not merged into {{< file "main" >}}. 

* The bug is present both in the release '''and''' {{< file "main" >}}. In this case create a hotfix branch from the latest release tag. If there is already a hotfix branch, you can either commit your changes directly there, or create a new branch (name it as you like) and put in a merge request with the existing hotfix branch as target. The hotfix branch then needs to be merged both in {{< file "main" >}} and be released with a new tag, so that the bug fix is applied to both. 

* The bug is '''only''' present in a release. This case is usually treated like if the bug also existed in {{< file "main" >}}. If the bug is not present in {{< file "main" >}}, it means that the lines of code where the bug was found have changed significantly in the {{< file "main" >}} branch. Therefore, the most likely outcome of merging the hotfix branch into {{< file "main" >}} is a merge conflict. In these circumstances these conflicts will most likely be trivial to solve. In any case, it is always worth trying to merge locally the fix into {{< file "main" >}} to see what happens beforehand.

###  Possible issues and special cases  

####  Bug fixes that have significantly diverged between a release and {{< file "main" >}}  

What if there is a bug that is present both in a release and {{< file "main" >}}, but the code where the bug is found has significantly diverged between the two branches? If the code in the release and {{< file "main" >}} have diverged to a point where they require two completely different sets of changes to fix the bug, treat this as two different bugs, one only exists in the release, and one that only exists in {{< file "main" >}}. Nevertheless, git is surprisingly good at applying the same patch to two different branches if the differences between the two branches are not too complex. So, before deciding to treat a case like this as two different bugs, you might want to try first to commit the fix to an hotfix branch created from the release tag, and then locally merging it into {{< file "main" >}} (make sure you know how to remove a commit by resetting the HEAD of a branch in git before attempting this).

####  Incorporating fixes into main before the hotfix branch is finished  

In the way Oneflow was originally designed, hotfix branches are supposed to only exist for a very short period of time. For a code like {{< octopus >}}, which is usually not a critical component of the users' system, we might want to accumulate a few bug fixes in the hotfix branch before doing a release. This is to avoid too frequent releases, and the associated work load. 

So what happens if one wants to include some fix from the hotfix branch into {{< file "main" >}} before the hotfix branch is finished? As it happens, one can merge the hotfix branch into {{< file "main" >}} at any time, and as often as necessary, so this is not really an issue. One simply needs to create merge requests to merge the hotfix branch into {{< file "main" >}} as necessary. 