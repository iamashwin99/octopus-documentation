---
Title: "How to write pages"
weight: 20
description: ""
---

## About this web page

The {{< octopus >}} documentation web pages are written in [markdown](https://www.markdownguide.org/basic-syntax/) and 
use [{{< hugo >}}](https://gohugo.io) as framework with the [hugo-theme-docdock](https://docdock.netlify.app/) theme.
{{< hugo >}} is a static webpage generator, which builds web pages from markdown sources.

For the {{< octopus >}} pages, we keep these markdown sources within the {{< octopus >}} source repository, which should encourage to write documentation whenever new code is added, and to update the documentation in case of code changes. The remaining files (e.g. configuration, shortcodes) required for the {{< hugo >}} system reside in a separate [repository](https://gitlab.com/octopus-code/octopus-documentation.git).


### The header

{{< hugo >}} requires a header on each page, which contains metadata about the page. Let's have a look at this example:

{{< code-block >}}
---
title: "Getting started"
description: "Learn how to run the code"
weight: 1
tutorials: ["Octopus Basics"]
theories: ["DFT"]
calculation_modes: ["Ground state"]
system_types: ["Molecule"]
species_types: ["Pseudopotentials"]
features: ["Total energy"]
utilities: []
difficulties: ["basic"]
---
{{< /code-block >}}
The minimum required header must contain:
{{< code-block >}}
---
title: "Getting started"
---
{{< /code-block >}}
Without these lines, a page will not be rendered to HTML.
The {{< code "description" >}} should be a one-line summary of the page, and {{< code "weight" >}} determines the order, in which pages are displayed in listings, with smaller number being displayed first.

The remaining entries are part of the so-called [taxonomy](https://gohugo.io/content-management/taxonomies/), and are used to assign pages to certain categories. See {{< tutorial "" "Tutorials" >}} as example.


### {{< hugo >}} shortcodes

{{< hugo >}} provides so-called [shortcodes](https://gohugo.io/templates/shortcode-templates/), which allow to define functions or macros, which can be used on a markdown page.

* {{< code "{{< octopus >}}" >}}: {{< octopus >}}
* {{< code "{{< octopus-version >}}" >}}: {{< octopus-version >}}
* {{< code "{{< emph \"text\" >}}" >}}: {{< emph "text" >}}
* {{< code "{{< code \"text\" >}}" >}}: {{< code "text" >}}
* {{< code "{{< code-line \"text\" >}}" >}}: {{< code-line "text" >}}
* {{< code "{{< file \"text\" >}}" >}}: {{< file "text" >}}
* {{< code "{{< command \"text\" >}}" >}}: {{< command "text" >}}
* {{< code "{{< command-line \"text\" >}}" >}}: {{< command-line "text" >}}
* {{< code "{{< name \"text\" >}}" >}}: {{< name "text" >}}

Some of these seem redundant, but they originate from porting the old wikimedia pages to the new framework.
Shortcodes as those above receive any content as argument. This, however, does not allow to render further markup or other shortcodes inside this content.
If this is required, one has to use shortcodes which come as a opening and closing shortcode. Some examples for these are:

* Code blocks:
<br>
{{< code "{{< code-block >}}" >}}<br>
{{< code "Example:" >}}<br>
{{< code "{{< octopus >}}  {{< octopus-version >}}" >}}<br>
{{< code "{{< /code-block >}}" >}}<br>
<br>
which will display as:
{{< code-block >}}
Example:
{{< octopus >}}  {{< octopus-version >}}
{{< /code-block >}}

* Inline code:
<br>
An inline code segment like {{< code "{{< code-inline >}}This is version {{< octopus-version >}}{{< /code-inline >}}" >}} will display the
text {{< code-inline >}}This is version {{< octopus-version >}}{{< /code-inline >}} inline with the paragraph.
<br>

Other usful shortcodes:

{{< expand "Units">}}

<ul>
<li> {{< code "{{< angstrom >}}" >}}: {{< angstrom >}}
<li> {{< code "{{< bohr >}}" >}}: {{< bohr >}}
<li> {{< code "{{< hartree >}}" >}}: {{< hartree >}}
</ul>

{{< /expand >}}

{{< expand "Compilation">}}

<ul>
<li> {{< code "{{< cc >}}" >}}: {{< cc >}}
<li> {{< code "{{< cflags >}}" >}}: {{< cflags >}}
<li> {{< code "{{< f90 >}}" >}}: {{< f90 >}}
<li> {{< code "{{< fcflags >}}" >}}: {{< fcflags >}}
<li> {{< code "{{< basedir >}" >}}: {{< basedir >}}
</ul>

{{< /expand >}}


</br>

### Automatically generated pages

The information about the variables is automatically generated from the source code. 


### Versioning of the documentation

The {{< hugo >}} system, together with the scripts to build the pages, can provide the documentation for different (major) versions of {{< octopus >}}.

If, for a given branch of the code, the documentation is contained in the folder {{< file "<basedir>/doc/hugo/" >}}, the contents of these folders will be copied into the {{< hugo >}} {{< file "content/" >}} folder, before the pages for that version are created. If this directory does not exist, the files will be taken from {{< file "default_content/" >}} within the octopus documentation repository.

#### Version dependent content

In general, content which is only for given versions of {{< octopus >}} *should* be organized through including the documentation in the {{< octopus >}} repository.
In case, version dependent content has to be added for older branches, which do not have the documentation in the code repository, there are the shortcodes:

- {{< code "{{< min-version \"version number\" >}}" >}} _Content_ {{< code "{{< /min-version >}}" >}}
- {{< code "{{< max-version \"version number\" >}}" >}} _Content_ {{< code "{{< /max-version >}}" >}}

#### Referencing other parts of the documentation

In order to create links to sections of the manual or tutorial, which refer to the correct {{< octopus >}} version, the following shortcodes are provided:

- {{< code "{{< manual \"Section:Name\" \"display text\" >}}" >}}: create a link to a manual page
- {{< code "{{< tutorial \"Section:Name\" \"display text\" >}}" >}}: create a link to a tutorial page
- {{< code "{{< developers \"Section:Name\" \"display text\" >}}" >}}: create a link to a developers page
- {{< code "{{< variable \"VariableName\" >}}" >}}: create a link to a variable reference
- {{< code "{{< versioned-link \"page address\" \"display text\" >}}" >}}: create a link to a page, where the address is relative to the top page of a given version.

### Including input files

Due to some limitations of Hugo, we cannot use shortcodes to include input files into a markdown file. Instead, this is handled by a shell script ( {{< code "build-pages.sh" >}}), which preprocesses markdown files, and inserts annotated input file. In a markdown file we can use

{{< command-line "#include_input filename" >}}

where {{< code filename >}} includes the path, relative to the {{< octopus >}} {{< basedir>}}.
This command copies the file from the {{< octopus >}} source tree into the hugo folders, and also annotates the file, i.e. is replaces all occurances of known variable names by the {{< code "{{< variable \"VariableName\" >}}" >}}, which produces a link to the corresponding variable reference.

{{< code-block >}}
{{< dummy "{{< code-block >}}">}}
{{< dummy "#include_input filename" >}}
{{< dummy "{{< /code-block >}}">}}
{{< /code-block >}}



{{< expand "Example" >}}

The following markdown excerpt

{{< code-block >}}
{{< dummy "{{< expand \"click to expand 'testsuite/tutorials/01-octopus_basics-getting_started.01-H_atom.inp'\" >}}">}}
{{< dummy "{{< code-block >}}">}}
{{< dummy "#include_input \"testsuite/tutorials/01-octopus_basics-getting_started.01-H_atom.inp\"" >}}
{{< dummy "{{< /code-block >}}">}}
{{< dummy "{{< /expand >}}">}}
{{< /code-block >}}

will generate the following result:</br>
</br>
 
{{< expand "click to expand 'testsuite/tutorials/01-octopus_basics-getting_started.01-H_atom.inp'" >}}
{{< code-block >}}
#include_input testsuite/tutorials/01-octopus_basics-getting_started.01-H_atom.inp
{{< /code-block >}}
{{< /expand >}}

{{< /expand >}}

</br>

{{< min-version 11 >}}
### Including input file snippets

In addition to complete input files, it is also possible to include a section, or snippet, of an input file.
The syntax is similar to the above, but uses the macro {<<code "#include_input_snippet">>}, as demonstrated in the following example:

{{< code-block >}}
{{< dummy "{{< expand \"click to expand 'octopus/testsuite/tutorials/01-octopus_basics-getting_started.01-H_atom.inp'\" >}}">}}
{{< dummy "{{< code-block >}}">}}
{{< dummy "#include_input testsuite/tutorials/01-octopus_basics-getting_started.01-H_atom.inp calc_mode" >}}
{{< dummy "{{< /code-block >}}">}}
{{< dummy "{{< /expand >}}">}}
{{< /code-block >}}

This requires the markers "{{<code "#snippet_start">}}{{<emph name>}}" and "{{<code "#snippet_end">}}{{<emph name>}}" 
in the input file. Due to the leading '#'. {{<octopus>}} treats them as comments and ignores those markers.

{{< expand "Example" >}}

The following markdown excerpt

{{< code-block >}}
{{< dummy "{{< code-block >}}">}}
{{< dummy "#include_input_snippet testsuite/tutorials/01-octopus_basics-getting_started.01-H_atom.inp calc_mode" >}}
{{< dummy "{{< /code-block >}}">}}
{{< /code-block >}}

will generate the following result:</br>
 
{{< code-block >}}
#include_input_snippet testsuite/tutorials/01-octopus_basics-getting_started.01-H_atom.inp calc_mode
{{< /code-block >}}

{{< /expand >}}

{{< /min-version >}}



### Including type definitions

Similar to the above macros to include some testfiles, there is also a macro to include the definition block of some user defined types.
Using
{{< code-block >}}
{{< dummy "#include_type_def <typename>" >}}
{{< /code-block >}}
extracts the corresponding source lines from the correct version of the code and inserts them into the markdown file, before further processing.
{{< notice note >}}
Note that no comments after the type name are allowed in the Fortran source. Otherwise the extraction preprocessor does not recognise the line.
{{< /notice >}}

To also use the Fortran source highlighting of {{< hugo >}} it is best to sandwich that by
{{< code-block >}}
{{< dummy "```Fortran" >}}
{{< dummy "```" >}}
{{< /code-block >}}

{{< expand "Example" >}}

The following markdown excerpt

{{< code-block >}}
{{< dummy "{{% expand \"click to expand the definition of batch_t\" %}}">}}
{{< dummy "```Fortran">}}
{{< dummy "#include_type_def batch_t" >}}
{{< dummy "```">}}
{{< dummy "{{% /expand %}}">}}
{{< /code-block >}}

will generate the following result:</br>
</br>
 
{{% expand "click to expand the definition of batch_t" %}}
```Fortran
#include_type_def batch_t
```
{{% /expand %}}


{{< notice note >}}
Note that the
{{< code-block >}}
{{< dummy "```Fortran" >}}
{{< dummy "```" >}}
{{< /code-block >}}
construction {{< emph only >}} works if the {{< emph expand >}} is given in the form {{< dummy "{{% expand ... %}} ... {{% /expand %}}" >}}. The
variant {{< dummy "{{< expand ... >}} ... {{< /expand >}}" >}} does not work.
{{< /notice >}}

{{< /expand >}}


### Including code snippets

Finally, there is a macro to include a general code snipped, which has to be marked in the source code as follows:
```Fortran
...
!# doc_start <name>
some code ...
!# doc_end
```


Using
{{< code-block >}}
{{< dummy "```Fortran" >}}
{{< dummy "#include_code_doc <name>" >}}
{{< dummy "```" >}}
{{< /code-block >}}
extracts the corresponding source lines from the correct version of the code and inserts them into the markdown file, before further processing.

### Including mathematical formulas

The {{<hugo>}} framework to generate the pages is set up to use **katex** to render mathematical formulas. The headers, which are silently included 
in every page contain scripts, which enable the rendering of formulas, typeset in *LaTeX*. Further information about **katex** can be found [here](https://katex.org).

In short, most *LaTeX* formulas can be inserted, either inline, enclosed by single '$' characters, or as separate lines, using double '$$'s.

{{% expand "Examples" %}}
```
The density is defined by $n({\bf r}) = \sum_i |\varphi_i({\bf r})|^2$, where the wavefunctions obey
$$
\left( -\frac{\nabla^2}{2 m} + v({\bf r}) \right) \varphi_i({\bf r}) = \epsilon_i \varphi_i({\bf r}) .
$$
```
The density is defined by $n({\bf r}) = \sum_i |\varphi_i({\bf r})|^2$, where the wavefunctions obey
$$
\left( -\frac{\nabla^2}{2 m} + v({\bf r}) \right) \varphi_i({\bf r}) = \epsilon_i \varphi_i({\bf r}) .
$$
{{% /expand %}}

{{% notice info %}}
In some cases, the equations do not render correctly. In this cases, it might be necessary to escape some control characters  (mainly the underscore) by an extra backslash.
For further details, see e.g. [this web page](https://discourse.gohugo.io/t/how-to-render-math-equations-properly-with-katex/40998)
{{% /notice %}}

### References

Two shortcodes for references are available.
{{< code-block >}}
{{< dummy "{{< article title=\"title\" authors=\"authors\" journal=\"journal\" volume=\"volume\" pages=\"pages\" year=\"year\" doi=\"doi\" >}}" >}}
{{< dummy "{{< book    title=\"title\" authors=\"authors\" publisher=\"publisher\" year=\"year\" pages=\"pages\" isbn=\"isbn\" >}}" >}}
{{< /code-block >}}

{{< expand "Examples" >}}
{{< article title=\"title\" authors=\"authors\" journal=\"journal\" volume=\"volume\" pages=\"pages\" year=\"year\" doi=\"doi\" >}} <br>
{{< book    title=\"title\" authors=\"authors\" publisher=\"publisher\" year=\"year\" pages=\"pages\" isbn=\"isbn\" >}}
{{< /expand >}}


Footnotes can be created by adding {{< code "[^label]" >}} in the text[^label]. The footnote content is then added as:
{{< code-block >}}
{{< dummy "[^label]: This is the footnote text." >}}
{{< /code-block >}}
and will be added at the bottom of the page.

[^label]: This is the footnote text.


