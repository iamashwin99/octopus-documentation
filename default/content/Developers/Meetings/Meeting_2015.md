---
title: "Meeting 2015"
weight: 10
---


##  Octopus developers meeting - 26-30 January 2015  
The second Octopus developers meeting took take place at the Friedrich-Schiller University of Jena, Germany from the 26 to the 30 of January 2015. 

###  Organizers  

Silvana Botti, Miguel Marques, Sylvia Hennig

We thank the Faculty of Physics and Astronomy of the University of Jena
for financial and logistical support.

###  List of participants  (arrival and departure dates)  

- Micael Oliveira (Monday afternoon - Friday afternoon)
- Miguel Marques (Monday afternoon - Friday afternoon)
- Silvana Botti
- Nicole Helbig (Monday afternoon - Friday afternoon)
- Alberto Castro (Tuesday morning - Friday morning)
- Joseba Alberdi (Monday evening - Friday midday)
- Alain Delgado Gran (Monday evening - Friday morning)
- Irina Lebedeva (Monday evening - Friday midday)
- Umberto De Giovannini (Monday evening - Thursday morning)
- Tiago Cerqueira 
- Rafael Sarmiento Pérez
- Ravindra Shinde (Monday evening - Friday midday)
- Matthieu Verstraete (Monday afternoon - Friday afternoon)
- Philipp Wopperer (Monday evening - Friday midday)
- Alejandro Varas (Monday evening - Friday midday)
- Iris Theophilou (Monday afternoon - Friday afternoon)
- Fernando Nogueira (Monday evening - Friday early morning)
- David Strubbe (Monday morning - Thursday morning)
- Rene Jestaedt
- Carlo Andrea Rozzi (Monday evening - Thursday morning)
- Tilman Dannert (Monday morning - Wednesday afternoon)

{{< figure src="images/P1280019.JPG" width="600px" >}}

###  Program  

How to add your slides: (1) click "Upload file" in the toolbox on the left of the screen. (2) Add a link like the ones below.

- Irina Lebedeva, Magneto-optical response of periodic insulators
- Alberto Castro, [[media:Talk-octopus2015-castro.pdf|What's new in optimal control]]
- Tim Baldsiefen/Jan Werschnik (Jenoptik), Short talk (15m), Real-world problems of interest in optics
- Iris Theophilou, RDMFT implementation in octopus [[media:rdmft_octopus_2015.pdf| The RDMFT implementation in Octopus]]
- Micael Oliveira, Short talk (10m), The CECAM Electronic Structure Library
- Alain Delgado Gran, [[media:pcm_octopus_reduced.pdf|Polarizable Continuum Model implementation in Octopus]]
- Rene Jestaedt, Real-time propagation of coupled Maxwell-Schrödinger and time-dependent Kohn-Sham-Maxwell systems
- David Strubbe, Octopus as starting point for GW/BSE calculations in BerkeleyGW
- David Strubbe, [[media:2014-12-14_MRS.pptx.pdf|Excited-state forces in TDDFT]]
- David Strubbe, [[media:Strubbe_Octopus_testsuite.pptx.pdf|The Octopus/APE/BerkeleyGW testsuite infrastructure]]
- David Hilditch, [[media:Numerical_Relativity_Introduction_Hilditch.pdf‎|Numerical general relativity]] (fun stuff that you can do with meshes)


|              | Monday         | Tuesday                          | Wednesday | Thursday        | Friday |
| :--:         | :---           | :---                             | :---      | :---            | :---   |
| **Morning**  |                | David (Testsuite infrastructure) | Alain  | David Hilditch     | Coding Party - IV |
|              |                | Coding Party - II                | Irina  | Coding Party - III |                   |
|              |                |                                  | Joseba |                    |                   |
|**Afternoon** | Welcome        | David (forces)                   | David (GW/BSE) |Visit to the Carl Zeiss factory | | 
|              | Coding Party I | Alberto                          | Iris           |                                | |
|              |                |                                  | Rene           |                                | |
|              |                |                                  | Micael         |                                | |
|              |                |                                  | Tim/Jan (short talk at 6pm) | | |
|**Evening**   |                |                                  | Dinner at Landgrafen |Let us go for beer | |

 
In the morning sessions will start at 9.00 (we are not a lazy bunch), coffee breaks at 10.30 (with wonderful cookies from Kahla, already tested by the organizers). Lunch is at 12.30 in the mensa. Afternoon sessions start at 13.30, coffee breaks at 15.00, end at 17.00.

The sessions will take place at the Konferenzraum (take the right stairs) in Max Wien Platz 1 (2nd floor). If you arrive Monday morning, you can find us in Silvana's offices in Fröbelstieg 1 (2nd floor).

{{< figure src="images/P1280021.JPG" width="600px" >}}

###  New reference  

Paper recently submitted: http://arxiv.org/abs/1501.05654

###  Discussions  

These are topics we should discuss during the meeting

- Release schedule: '''Decision to release every year on the 1st of February plus eventual bug-fix releases every few months.'''
- Making sure all people doing development have commit access, and are committing their work regularly '''Accepted'''
- Adoption of Fortran 2003 features (a list of compiler support can be found [http://fortranwiki.org/fortran/show/Fortran+2003+status here]) '''Accepted'''
- Removal of the "frozen" code '''Not yet'''
- Removal of the open-quantum-systems code '''Accepted'''
- Removal of the "datasets" feature '''Accepted'''
- Removal of the single precision code '''Accepted'''
- Suggestion by Pablo García Risueño to change the default Poisson solver '''To be discussed later, after the new libisf is included in the code'''
- Changing the documention and wiki content license from GNU License to CC-BY '''Decision to do it, pending approval by other major contributors'''
- Migrate from SVN to Bazaar '''Rejected'''
- Make pseudopotential filtering the default '''Accepted'''
- Remove support for Siesta pseudopotential format '''Accepted'''
- Add complete table of pseudopotentials ([http://arxiv.org/abs/1502.00995 link]) '''Accepted'''

###  Coding sessions  

Here is a list of proposed topics for the coding party. Feel free to add more items to the list.
- Detect unused variables in input (Miguel) and update interface to F2003 release and correct liboct_parser 2.0 ('''Miguel, Micael''').
- Check the [[Developers:Starting_to_develop | new page]] for new developers for things missing and tell Nicole ('''everyone''').
- Refactoring of the test farm ('''Micael, Alejandro''')
- Migrating www.tddft.org to a new machine ('''Fernando, Miguel''')
- Merge the different versions of the Octopus/APE/BerkeleyGW testsuite scripts ('''David, Micael''')
- For partially periodic systems, fix Poisson solver and implement Ewald sums ([http://kfe.fjfi.cvut.cz/~klimo/mpf/Ewald_notes.pdf ref]) Note: With the new interpolation code it is possible to simplify the calculation by using the Hartree potential. ('''Carlo''') 
- Implementation of the new LibISF API & start the extraction of the code, to include it as external library ('''Joseba, Micael''')
- Sort out whether there is any real issue regarding this warning: "Multiple periodic replicas in the same region will not be considered"
- Review, update, and extend tutorials ('''Pedro Borlido, Matthieu/Nicole''') and complete tutorial of solids ('''David''')
- Parallelization of photoemission ('''Tilman, Umberto, David''')
- Revise coding standards ('''David''') -- ''DONE'' [[Developers:Coding standards]]
- See whether everything set as experimental really needs to be
- Non-orthogonal unit-cells (formulas can be found [http://journals.aps.org/prb/pdf/10.1103/PhysRevB.78.075109 here]) ('''Ravindra, Matthieu, Umberto''')
- Use fxc from libxc for GGAs (the formula can be found [http://journals.aps.org/prb/pdf/10.1103/PhysRevB.69.115106 here] and [http://www.tddft.org/TDDFT2004/PracticalSessions/papers/ahlrichs_casida.pdf here]) ('''Fernando''')
- Exchange term in Casida for TD Hartree-Fock or hybrids ('''David, Nicole, Iris, Alain''')
- Constraints in geometry optimization (make a real C library out of [https://trac.fysik.dtu.dk/projects/ase/browser/trunk/ase/constraints.py this]) ('''Alejandro, Miguel''')
- Parallelize exact exchange in states, by merging with OEP implementation
- Make LCAO work properly for spinors
- Make Sternheimer equation parallel in states  ('''David''')
- Start PAW implementation
- Range-separated hybrids (''e.g.'' HSE, CAM-B3LYP, PBE0) 
- Implement Swedish van der Waals functionals (Soler's formulation is [http://arxiv.org/abs/0812.0244 here])
- Implement Tkatchenko's van der Waals functional

###  Practical information  

The meeting will be in [Max Wien Platz 1](https://maps.google.com/maps?q=Max-Wien-Platz+1,+Jena,+Germany&hl=en&ll=50.93407,11.583954&spn=0.001114,0.002216&sll=37.0625,-95.677068&sspn=45.601981,72.597656&oq=Max+Wien+Platz+1&t=h&hnear=Max-Wien-Platz+1,+07743+Jena,+Germany&z=19)  in the Conference Room. Silvana's group is located 100 meters away in another building. You  can find information how to reach it [here](http://www.ifto.uni-jena.de/en/find.html).

The [hotel](http://en.steigenberger.com/Jena/Steigenberger-Esplanade) is [here](https://maps.google.de/maps?q=steigenberger+esplanade+hotel+jena&ll=50.927414,11.581897&spn=0.002228,0.004431&oe=utf-8&client=firefox-a&fb=1&gl=de&cid=15124743449003887317&t=h&z=18).

Notice that Jena is a very small (and charming) city, so you can basically go everywhere walking.
