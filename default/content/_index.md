---
title: "Octopus Documentation Homepage"
author: "Martin Lueders"
section: "Octopus"
weight: 1
hiden: true
---

# Octopus

{{< octopus >}} is a scientific program aimed at the ab initio virtual experimentation on a hopefully ever-increasing range of system types. Electrons are described quantum-mechanically within density-functional theory (DFT), in its time-dependent form (TDDFT) when doing simulations in time. Nuclei are described classically as point particles. Electron-nucleus interaction is described within the pseudopotential approximation.

For optimal execution performance {{< octopus >}} is parallelized using MPI and OpenMP and can scale to tens of thousands of processors. It also has support for graphical processing units (GPUs) through OpenCL and CUDA.

{{< octopus >}} is free software, released under the GPL license, so you are free to download it, use it and modify it.


{{% notice note %}}
The new web page is still under construction. Not all pages are up-to-date. If you want to check against the old web content, you can access the wiki [here](https://octopus-code.org/wiki).
Please, send reports about wrong or missing information, as well as other suggestions to the {{<octopus>}} developers.
{{% /notice %}}

# News:

Latest release: {{<releases-latest>}}

# Octopus mailing lists:

The {{<octopus>}} community utilizes three mailing lists:

* octopus-announce@lists.octopus-code.org: any kind of announcements, e.g. new release, workshops, etc.
* octopus-users@lists.octopus-code.org: getting help regarding the use of {{<octopus>}}
* octopus-devel@lists.octopus-code.org: closed list for discussions amongst developers.

In order to sign up with one or more of these mailing lists, visit:
* https://listserv.gwdg.de/mailman/listinfo/octopus-announce
* https://listserv.gwdg.de/mailman/listinfo/octopus-users
* https://listserv.gwdg.de/mailman/listinfo/octopus-devel

# Development

* The {{< versioned-link "Octopus_Developers" "Octopus Developers Team" >}}
* The [Buildbot web interface](https://octopus-code.org/buildbot)
* The [testsuite app](https://octopus-code.org/testsuite)
# Useful info:

* {{< versioned-link "Citing_Octopus" "How to cite Octopus" >}}
* {{< versioned-link "Books" "Books" >}}
* {{< versioned-link "Articles" "Articles" >}}
* {{< versioned-link "Links" "Links" >}}

# Related projects

* [libxc](https://www.tddft.org/programs/libxc//)
* [fortrancl](https://code.google.com/p/fortrancl)