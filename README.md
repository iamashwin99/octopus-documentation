---
Title: "Building the documentation"
Weight: 5
description: ""
---


If you are writing new material for the tutorials or the manual, it is helpful to build the documentation pages locally, before submitting the material to the {{< octopus >}} web server. All that is needed to build the documentation are {{< hugo >}} and a web server, such as [apache](http://httpd.apache.org/)

If you already have installed {{< hugo >}} and started the web server, you can continue with {{< versioned-link "developers/building_docs/#building-the-pages" "building the pages" >}}. Otherwise, follow the instructions to install {{< hugo >}} and set up a web server.

## Installing {{< hugo >}}

The documentation system requires {{< hugo >}} at least at version 0.79. {{< hugo >}} can be downloaded [here](https://github.com/gohugoio/hugo). For most operating systems, a binary version can be found [here](https://github.com/gohugoio/hugo/releases).

Once the package is installed, you can check the correct version by
```bash
hugo version
```



## Installing a web server

The easiest option is to use a docker image for the web server.

Here we assume that you already have docker on your system or know how to install it. In case, you need support, see the official documentation on
[installing docker](https://docs.docker.com/engine/install/)


### Installing the httpd docker image

Once docker is successfully installed on the system, one can simply pull a pre-defined docker image, which provides the apache2 webserver.
For this, use the command:
```bash
docker pull httpd
```
which will download the {{< code httpd >}} image.


### Starting the docker image:

We assume that {{< code "$HUGO_DOC" >}} is the directory, in which you will clone the documentation repository.

```bash
docker run -dit -p 8080:80 -v "$HUGO_DOC/Octopus/public":/usr/local/apache2/htdocs/ httpd
```

## Cloning the repository

The documentation framework is kept in a git repository at [MPSD](https://mpsd-gitlab2.desy.de). 
This repository contains all necessary files (such as the shortcodes, layouts, etc.) and contains the docdock theme as a submodule.
Also the octopus code repository is a submodule of the repository. This is required as the source files as well as testfiles will be parsed while building the documentation, and also since from {{< octopus >}} version 11, all documentation source (i.e. markdown) files will be part of the {{< octopus >}} source repository.

```bash
cd $HUGO_DOC
git clone <repo-name>

cd Octopus
git submodule update --init --recursive
```



## Building the pages

Building the web pages is done by the script {{< code "build-pages.sh" >}}, which performs the following steps for each selected code version (branch):

* Check out the latest version of each specified {{< octopus >}} branch
* If present, copy {{< basedir >}}/doc/html/content/* to {{< file "$HUGO_DOC/Octopus/content/" >}}
* Parse all {{< octopus >}} source files and build the {{< file "variables.json" >}} file, containing all variable information.
* Parse testsuite input files, as well as tutorial and manual files to create links to variable descriptions.
* Create the variable reference markdown files from thes above information.
* Run hugo to create the static pages.

The {{< octopus >}} versions for which the documentation is built are specified in the file {{< file "branches" >}}, which should look like:
```text
# branch name
#
9.2 9
10.3 10
develop develop
```
where the first entry in each line is the name of the {{< name tag >}} of an {{< octopus >}} version, while the second entry is the name under which it should appear on the web pages.
{{< notice note >}}
The order of the lines is important, as the first corresponds to the left-most entry in the Version selector in the header of the web pages. Line can be commented out with a hash, but there must not be any empty lines in the file.
{{< /notice >}}

{{< notice warning >}}
Be aware that the {{< file build-pages.sh >}} script, in general, will checkout the latest version of the {{< octopus >}} repository from git. If you have local changes, which are not committed, they might be lost.
{{< /notice >}}

{{% notice tip %}}
If you are developing tutorial pages and do not want to build all pages, you can leave use the {{< file rebuild-last.sh >}} script.
This will not touch the {{< octopus >}} code repository, but _only_ generate the web pages for the last entry of the {{< file branches >}} file.
It is important, thought, that the full script {{< file build-pages.sh >}} has been run at least once before {{< file rebuild-last.sh >}} can be used.
{{% /notice %}}
